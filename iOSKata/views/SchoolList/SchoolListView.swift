//
//  SchoolListView.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

import SwiftUI

struct SchoolListView: View {
    @ObservedObject var viewModel = SchoolListViewModel()
    
    var body: some View {
        return VStack {
                if viewModel.currentItems.count == 0 {
                    Text("Loading...")
                } else {
                    VStack {
                        List(viewModel.currentItems) { item in
                            NavigationLink(destination: SatScoreView(itemID: item.id)) {
                                VStack(alignment: .leading) {
                                    Text(item.schoolName)
                                        .font(.system(size: 18))
                                    
                                    Text("Total students: \(item.totalStudents ?? "0")")
                                        .font(.system(size: 12))
                                }
                            }
                        }
                    }
                }
            }
            .navigationBarItems(leading: HStack {
                if viewModel.showBackButton {
                    Button(action: {
                        self.viewModel.previousIndex()
                    }, label: {
                        Text("Previous")
                    })
                }
            }, trailing: HStack {
                if viewModel.showForwardButton {
                    Button(action: {
                        self.viewModel.nextIndex()
                    }, label: {
                        Text("Next")
                    })
                }
            })
            .navigationBarTitle("NYC School List", displayMode: .inline)
    }
}
