//
//  SchoolListModel.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

struct SchoolListModel: Codable, Identifiable {
    var id: String
    var schoolName: String
    var totalStudents: String?
    
    private enum CodingKeys : String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case totalStudents = "total_students"
    }
}
