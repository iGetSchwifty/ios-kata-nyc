//
//  SchoolListService.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//
import Foundation
import Combine

class SchoolListService {
    static func fetch(limit: Int, offset: Int, provider: NetworkingProtocol) -> AnyPublisher<[SchoolListModel], Never> {
        guard let url = URL(string: URLService.schools(limit: limit, offset: offset)) else {
            // TODO: Log this if it wasnt a kata
            return Just([]).eraseToAnyPublisher()
        }
        
        return provider.dataTaskPublisher(for: url)
            .map({ response -> [SchoolListModel] in
                do {
                    return try JSONDecoder().decode([SchoolListModel].self, from: response)
                } catch let error {
                    // we could log the error, send it back in the publisher and let whoever consumes it handle
                    // like popping an alert since this is only a kata though we print it...
                    print(error)
                    return []
                }
            })
            .replaceError(with: [])
            .eraseToAnyPublisher()
    }
}
