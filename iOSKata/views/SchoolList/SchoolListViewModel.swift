//
//  SchoolListViewModel.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//
import Foundation
import Combine

class SchoolListViewModel: ObservableObject {
    private var disposeBag = Set<AnyCancellable>()
    
    @Published var showBackButton: Bool = false
    @Published var showForwardButton: Bool = true
    @Published var currentItems = [SchoolListModel]()
    
    var currentIndex = 0
    private let itemLimit = 10
    private let provider: NetworkingProtocol
    
    init(provider: NetworkingProtocol = NetworkingPublisher()) {
        self.provider = provider
        fetchItems(index: currentIndex)
    }
    
    private func fetchItems(index: Int) {
        SchoolListService.fetch(limit: itemLimit,
                                offset: offsetFrom(index: index),
                                provider: provider)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] response in
                guard let self = self else { return }
                self.updateButtons(index: index, count: response.count)
                self.currentItems = response
            }.store(in: &disposeBag)
    }
    
    func nextIndex() {
        currentIndex += 1
        fetchItems(index: currentIndex)
    }
    
    func previousIndex() {
        if currentIndex > 0 {
            currentIndex -= 1
        }
        fetchItems(index: currentIndex)
    }
    
    // in order to handle the api there isnt a value that I can see of total page size without calling the whole api.
    // since this is a small kata i didnt really mind if the next button was showing but there was no data next
    // also if they hit the button and there are no items. It should just hide due to the count being zero
    private func updateButtons(index: Int, count: Int) {
        if index > 0 {
            self.showBackButton = true
        } else {
            self.showBackButton = false
        }
        
        if count == 0 {
            self.showForwardButton = false
        } else {
            self.showForwardButton = true
        }
    }
    
    func offsetFrom(index: Int) -> Int {
        return itemLimit * index
    }
}
