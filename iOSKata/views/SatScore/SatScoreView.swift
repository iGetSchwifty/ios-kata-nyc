//
//  SatScoreView.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

import SwiftUI

struct SatScoreView: View {
    @ObservedObject var viewModel: SatScoreViewModel
    
    init(itemID: String) {
        viewModel = SatScoreViewModel(itemID: itemID)
    }
    
    var body: some View {
        VStack {
            if viewModel.currentItem == nil {
                Text("No data")
            } else {
                VStack {
                    Text(viewModel.currentItem?.schoolName ?? "")
                        .font(.system(size: 24)).padding()
                    
                    Text("Total SAT Takers: \(viewModel.currentItem?.numberOfTestTakers ?? "0")")
                        .font(.system(size: 14))
                }.padding()
                
                Text("Average Scores").padding()
                
                Text("Reading: \(viewModel.currentItem?.readingAverage ?? "0")")
                    .font(.system(size: 14))
                
                Text("Math: \(viewModel.currentItem?.mathAverage ?? "0")")
                    .font(.system(size: 14))
                
                Text("Writing: \(viewModel.currentItem?.writingAverage ?? "0")")
                    .font(.system(size: 14))
            }
        }
        .onAppear {
            self.viewModel.fetch()
        }
    }
}
