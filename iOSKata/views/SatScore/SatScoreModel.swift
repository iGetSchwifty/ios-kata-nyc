//
//  SatScoreModel.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

struct SatScoreModel: Codable, Identifiable {
    var id: String
    var schoolName: String
    var numberOfTestTakers: String
    var readingAverage: String
    var mathAverage: String
    var writingAverage: String
    
    private enum CodingKeys : String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case readingAverage = "sat_critical_reading_avg_score"
        case mathAverage = "sat_math_avg_score"
        case writingAverage = "sat_writing_avg_score"
    }
}
