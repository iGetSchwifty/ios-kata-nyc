//
//  SatScoreService.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//
import Foundation
import Combine

class SatScoreService {
    static func fetch(id: String, provider: NetworkingProtocol) -> AnyPublisher<SatScoreModel?, Never> {
        guard let url = URL(string: URLService.satResultsForSchool(id: id)) else {
            // TODO: Log this if it wasnt a kata
            return Just(nil).eraseToAnyPublisher()
        }
        
        return provider.dataTaskPublisher(for: url)
            .map({ response -> SatScoreModel? in
                do {
                    return try JSONDecoder().decode([SatScoreModel].self, from: response).first
                } catch let error {
                    // we could log the error, send it back in the publisher and let whoever consumes it handle
                    // like popping an alert since this is only a kata though we print it...
                    print(error)
                    return nil
                }
            })
            .replaceError(with: nil)
            .eraseToAnyPublisher()
    }
}
