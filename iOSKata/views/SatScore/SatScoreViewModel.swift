//
//  SatScoreViewModel.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

import Foundation
import Combine

class SatScoreViewModel: ObservableObject {
    @Published var currentItem: SatScoreModel?
    
    private var disposeBag = Set<AnyCancellable>()
    private let itemID: String
    private let provider: NetworkingProtocol
    
    init(itemID: String, provider: NetworkingProtocol = NetworkingPublisher()) {
        self.itemID = itemID
        self.provider = provider
    }
    
    func fetch() {
        SatScoreService.fetch(id: itemID, provider: provider)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] model in
                guard let self = self,
                      let model = model else {
                    return
                }
                self.currentItem = model
            }.store(in: &disposeBag)
    }
}
