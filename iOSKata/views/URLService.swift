//
//  URLService.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

class URLService {
    static func schools(limit: Int, offset: Int) -> String {
        return "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=\(limit)&$offset=\(offset)"
    }
    
    static func satResultsForSchool(id: String) -> String {
        return "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(id)"
    }
}
