//
//  ContentView.swift
//  iOSKata
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

import SwiftUI

struct AppView: View {
    var body: some View {
        NavigationView {
            SchoolListView()
        }
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
