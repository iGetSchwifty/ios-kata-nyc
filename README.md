# NYC iOS Kata

iOS Kata for viewing NYC school data.

Tried to time box myself to one evening. Im moving into my house this weekend and didnt want to spend more than one evening on this kata as I have a lot of packing to do.

With that being said I tried to follow a MVVM approach while using Combine and SwiftUI. I can easily test my ViewModels as well as services to make sure that I have proper unit test coverage of my code. I have choosen SwiftUI and Combine as it is the latest apple technologies that allow for reactive programming paradigms to be easily implemented.

I didnt spend any time at all making the UI pretty other than making sure that it appeared on the screen. That being said there is no polish in terms of loading views or activity indicators to demonstrate that the app is fetching or processing data.

What was important to me was making sure that I had functional but testable code. It said in the kata that I should not spend time on making it look pretty so I didnt spend time on that. However, in a production application making sure something matches UI requirments is very imporant to me as well as functionality.

I also didnt give the user any option to change the batching limit for the school request. Since the api could potentially return a lot of data which would slow down the app when consumed I decided to batch this request. I also am not caching this data in anyway (Using CoreData or just an in memory cache) I decided on a hardcoded limit of 10 school results at a time. I added a Next Button and a previous button to page through the data. Ideally in a production application the decision to fetch more data would be made on a scroll offset or some server response to invalidate client side cache. I also didnt build any user exposed searching functionality.

All of that said it was a fun little kata and I hope you enjoy the code that I wrote for it. 

![](demo.mov)