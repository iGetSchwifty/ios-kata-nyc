//
//  SchoolListViewModelTests.swift
//  iOSKataTests
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

import Combine
import XCTest
@testable import iOSKata

class SchoolListViewModelTests: XCTestCase {
    var disposeBag = Set<AnyCancellable>()
    func test_init() {
        let mockData = [SchoolListModel(id: "1", schoolName: "First", totalStudents: "42"),
                        SchoolListModel(id: "2", schoolName: "Second", totalStudents: "4")]
        MockNetworking.mockData = try? JSONEncoder().encode(mockData)
        let expectation = XCTestExpectation(description: "api called with mock data")
        let viewModel = SchoolListViewModel(provider: MockNetworking())
        viewModel.$currentItems.sink { response in
            guard response.count == 2 else { return }
            XCTAssertEqual(response.count, 2)
            XCTAssertEqual(response[0].id, "1")
            XCTAssertEqual(response[0].schoolName, "First")
            XCTAssertEqual(response[0].totalStudents, "42")
            XCTAssertEqual(response[1].id, "2")
            XCTAssertEqual(response[1].schoolName, "Second")
            XCTAssertEqual(response[1].totalStudents, "4")
            expectation.fulfill()
        }.store(in: &disposeBag)
        wait(for: [expectation], timeout: 10)
    }
    
    func test_nextIndex() {
        let viewModel = SchoolListViewModel(provider: MockNetworking())
        XCTAssertEqual(viewModel.currentIndex, 0)
        viewModel.nextIndex()
        viewModel.nextIndex()
        XCTAssertEqual(viewModel.currentIndex, 2)
    }
    
    func test_previousIndex() {
        let viewModel = SchoolListViewModel(provider: MockNetworking())
        viewModel.previousIndex()
        XCTAssertEqual(viewModel.currentIndex, 0)
        viewModel.nextIndex()
        viewModel.nextIndex()
        viewModel.previousIndex()
        XCTAssertEqual(viewModel.currentIndex, 1)
    }
    
    func test_offsetFrom() {
        let viewModel = SchoolListViewModel(provider: MockNetworking())
        XCTAssertEqual(viewModel.offsetFrom(index: 0), 0)
        XCTAssertEqual(viewModel.offsetFrom(index: 1), 10)
        XCTAssertEqual(viewModel.offsetFrom(index: 2), 20)
        
    }
}
