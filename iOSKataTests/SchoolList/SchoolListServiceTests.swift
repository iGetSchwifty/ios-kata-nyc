//
//  SchoolListServiceTests.swift
//  iOSKataTests
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

import Combine
import XCTest
@testable import iOSKata

class SchoolListServiceTests: XCTestCase {
    var disposeBag = Set<AnyCancellable>()
    func test_fetch() {
        let mockData = [SchoolListModel(id: "1", schoolName: "First", totalStudents: "42"),
                        SchoolListModel(id: "2", schoolName: "Second", totalStudents: "4")]
        MockNetworking.mockData = try? JSONEncoder().encode(mockData)
        let expectation = XCTestExpectation(description: "api called with mock data")
        SchoolListService.fetch(limit: 0, offset: 0, provider: MockNetworking())
            .sink { response in
                XCTAssertEqual(response.count, 2)
                XCTAssertEqual(response[0].id, "1")
                XCTAssertEqual(response[0].schoolName, "First")
                XCTAssertEqual(response[0].totalStudents, "42")
                XCTAssertEqual(response[1].id, "2")
                XCTAssertEqual(response[1].schoolName, "Second")
                XCTAssertEqual(response[1].totalStudents, "4")
                expectation.fulfill()
            }.store(in: &disposeBag)
        wait(for: [expectation], timeout: 10)
    }
}
