//
//  iOSKataTests.swift
//  iOSKataTests
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//

import XCTest
@testable import iOSKata

class URLServiceTests: XCTestCase {
    func test_schools() {
        let limit = 42
        let offset = 2
        XCTAssertEqual(URLService.schools(limit: limit,
                                          offset: offset),
                       "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=42&$offset=2")
    }
    
    func test_satResultsForSchool() {
        let testID = "42"
        XCTAssertEqual(URLService.satResultsForSchool(id: testID),
                       "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=42")
    }
}
