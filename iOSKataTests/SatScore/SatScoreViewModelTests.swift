//
//  SatScoreViewModelTests.swift
//  iOSKataTests
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//


import Combine
import XCTest
@testable import iOSKata

class SatScoreViewModelTests: XCTestCase {
    var disposeBag = Set<AnyCancellable>()
    func test_fetch() {
        let mockData = [SatScoreModel(id: "42", schoolName: "Test", numberOfTestTakers: "4", readingAverage: "22", mathAverage: "2", writingAverage: "222")]
        MockNetworking.mockData = try? JSONEncoder().encode(mockData)
        let expectation = XCTestExpectation(description: "api called with mock data")
        let viewModel = SatScoreViewModel(itemID: "42", provider: MockNetworking())
        viewModel.fetch()
        viewModel.$currentItem.sink { response in
            guard response != nil else { return }
            XCTAssertEqual(response?.id, "42")
            XCTAssertEqual(response?.schoolName, "Test")
            XCTAssertEqual(response?.numberOfTestTakers, "4")
            XCTAssertEqual(response?.readingAverage, "22")
            XCTAssertEqual(response?.mathAverage, "2")
            XCTAssertEqual(response?.writingAverage, "222")
            expectation.fulfill()
        }.store(in: &disposeBag)
        wait(for: [expectation], timeout: 10)
    }
}
