//
//  MockNetworking.swift
//  iOSKataTests
//
//  Created by Jeff on 5/13/20.
//  Copyright © 2020 Jeffrey. All rights reserved.
//
import Combine
import Foundation
@testable import iOSKata

class MockNetworking: NetworkingProtocol {
    static var mockData: Data?
    
    func dataTaskPublisher(for request: URL) -> AnyPublisher<Data, URLSession.DataTaskPublisher.Failure> {
        return session.dataTaskPublisher(for: request)
            .map { _ in
                return MockNetworking.mockData ?? Data()
            }.eraseToAnyPublisher()
    }

    var session: URLSession

    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
}
